use crate::gpio::Pin;
pub struct Peripharals{
    /// physical pin 1
    pub pin1:Pin<0>,
    /// physical pin 2
    pub pin2:Pin<1>,
    /// physical pin 4
    pub pin4:Pin<2>,
    /// physical pin 5
    pub pin5:Pin<3>,
    /// physical pin 6
    pub pin6:Pin<4>,
    /// physical pin 7
    pub pin7:Pin<5>,
    /// physical pin 9
    pub pin9:Pin<6>,
    /// physical pin 10
    pub pin10:Pin<7>,
    /// physical pin 11
    pub pin11:Pin<8>,
    /// physical pin 12
    pub pin12:Pin<9>,
    /// physical pin 14
    pub pin14:Pin<10>,
    /// physical pin 15
    pub pin15:Pin<11>,
    /// physical pin 16
    pub pin16:Pin<12>,
    /// physical pin 17
    pub pin17:Pin<13>,
    /// physical pin 19
    pub pin19:Pin<14>,
    /// physical pin 20
    pub pin20:Pin<15>,
}
