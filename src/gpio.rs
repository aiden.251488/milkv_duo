use linux_embedded_hal as linux_hal;
pub struct Pin<const N:usize>(pub linux_hal::Pin);
